# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 17:24:14 2018

@author: enovi
"""

#Combo Analyzer
#Analyze every text file in a folder with multiple scripts
#Or analyze selected files

import nltk

def main():
    location = input('Enter location of text files: ')
    c = input('Use filename filter y/n? ').lower()
    if c == 'y':
        f = input('Enter the keyword in selected filenames: ')
        texts = nltk.corpus.PlaintextCorpusReader(location, '{}*.*xt'.format(f))
    else:
        texts = nltk.corpus.PlaintextCorpusReader(location, '.*xt')

    display = True
    
    c = input('Show results for individual articles y/n? ')
    if c == 'y':
        display = True
    else:
        display = False
    
    print('Select analyzer scripts to run: ')
    c = input('Run all scripts y/n? ').lower()
    if c == 'y':
        print('Running all scripts')
    else:    
        c = input('Run fan analyzer y/n? ').lower()
        if c == 'y':
            c1 = True
        else:
            c1 = False
        c = input('Run objective/subjective y/n? ').lower()
        if c == 'y':
            c2 = True
        else:
            c2 = False
        c = input('Run Big 5/OCEAN y/n? ').lower()
        if c == 'y':
            c3 = True
        else:
            c3 = False
        c = input('Run urgency y/n? ').lower()
        if c == 'y':
            c4 = True
        else:
            c4 = False
        c = input('Run status y/n? ').lower()
        if c == 'y':
            c5 = True
        else:
            c5 = False
        c = input('Run narrative y/n? ').lower()
        if c == 'y':
            c6 = True
        else:
            c6 = False
        c = input('Run additional support y/n? ').lower()
        if c == 'y':
            c7 = True
        else:
            c7 = False
        c = input('Run automated readability index y/n? ').lower()
        if c == 'y':
            c8 = True
        else:    
            c8 = False
        c = input('Run scientific terms y/n? ').lower()
        if c == 'y':
            c9 = True
        else:    
            c9 = False
        c = input('Run stopwords ratio y/n? ').lower()
        if c == 'y':
            c10 = True
        else:    
            c10 = False
        c = input('Run storytelling ratio y/n? ').lower()
        if c == 'y':
            c11 = True
        else:
            c11 = False
        c = input('Run cooperation y/n? ').lower()
        if c == 'y':
            c12 = True
        else:
            c12 = False
    
    if c1 == True:        
        print('Fan Analyzer: Team, Emotion, Activity, Action, Subculture')
        
        team     = 0
        emotion  = 0
        activity = 0
        action   = 0
        subcult  = 0
        fan      = 0
        
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = fan_analyzer(article_text,fileid,display)
            team     += l[0]
            emotion  += l[1]
            activity += l[2]
            action   += l[3]
            subcult  += l[4]
            fan      += 1
        if fan > 0:    
            print(team/fan,emotion/fan,activity/fan,action/fan,subcult/fan,fan)                    
                       
    if c2 == True:
        print('Objective, Subjective Words')
        obj     = 0
        subj    = 0
        obsub   = 0
        
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = objsubj(article_text,fileid,display)
            obj     += l[0]
            subj    += l[1]
            obsub   += 1
        if obsub > 0:
            print(obj/obsub,subj/obsub,obsub)
    
    if c3 == True:
        print('Big 5: Openness, Conscientiousness, Extraversion, Agreeableness, Neuroticism')
        ope = 0
        con = 0
        ext = 0
        agr = 0
        neu = 0
        big = 0
        
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = psychological_analyzer(article_text,fileid,display)
            ope += l[0]
            con += l[1]
            ext += l[2]
            agr += l[3]
            neu += l[4]
            big += 1
        if big > 0:
            print(ope/big,con/big,ext/big,agr/big,neu/big,big)
                
    if c4 == True:    
        print('Urgency')
        urg = 0
        urc = 0
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = urgency_analyzer(article_text,fileid,display)
            urg += l[0]
            urc += 1
        if urc > 0:    
            print(urg/urc,urc)
    
    if c5 == True:    
        print('Status')
        sta = 0
        stc = 0
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = status_analyzer(article_text,fileid,display)
            sta += l[0]
            stc += 1
        if stc > 0:
            print(sta/stc,stc)
    
    if c6 == True:    
        print('Narrative')
        nar = 0
        nrc = 0
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = narrative_analyzer(article_text,fileid,display)
            nar += l[0]
            nrc += 1
        if nrc > 0:
            print(nar/nrc,nrc)   
    
    if c7 == True:    
        print('Additional Support')
        add = 0
        adc = 0        
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = addition_analyzer(article_text,fileid,display)
            add += l[0]
            adc += 1
        if adc > 0:    
            print(add/adc,adc)            
            
    if c8 == True:
        print('Automated Readability Index')
        ari = 0
        arc = 0        
        for fileid in texts.fileids():
            chars        = texts.raw(fileid)
            article_text = texts.words(fileid)
            sents        = texts.sents(fileid)
            l = automatedreadability(chars,article_text,sents,fileid,display)
            ari += l[0]
            arc += 1
        if arc > 0:
            print(ari/arc,arc)           
            
    if c9 == True:
        print('Scientific Terms')
        sou  = 0
        qua  = 0
        cert = 0
        sci  = 0
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = news_analyzer(article_text,fileid,display)
            sou  += l[0]
            qua  += l[1]
            cert += l[2]
            sci  += 1
        if sci > 0:
            print(sou/sci,qua/sci,cert/sci,sci)
    
    if c10 == True:        
        print('Stopwords Ratio')
        sto = 0
        stc = 0
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            l = stops(article_text,fileid,display)
            sto += l[0]
            stc += 1
        if stc > 0:
            print(sto/stc,stc)
            
    if c11 == True:
        print('Storytelling')
        cause        = 0
        follow       = 0        
        narr         = 0
        
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = storytelling(article_text,fileid,display)
            cause         += l[0]
            follow        += l[1]
            narr          += 1
        if narr > 0:
            print(cause/narr,follow/narr,narr)
            
    if c12 == True:        
        print('Cooperation')
        
        fami     = 0
        grou     = 0
        reci     = 0
        hero     = 0
        defe     = 0
        fair     = 0
        prop     = 0
        article  = 0 
        
        for fileid in texts.fileids():
            article_text = texts.words(fileid)
            for word in article_text:
                word = nltk.PorterStemmer().stem(word)
            l = cooperation(article_text,fileid,display)
            fami     += l[0]
            grou     += l[1]
            reci     += l[2]
            hero     += l[3]
            defe     += l[4]
            fair     += l[5]
            prop     += l[6]
            article  += 1
        if article > 0:    
            print(fami/article, grou/article, reci/article, hero/article, 
                  defe/article, fair/article, prop/article, article)        
    
def fan_analyzer(article_text,fileid,display):
    #team orientation, emotional impact, activity orientation, action orientation, subculture orientation
    team_list     = ['addict', 'adherent', 'admirer', 'advocate', 'aficionado', 'ally',
                     'apologist', 'backer', 'believer', 'benefactor', 'buff', 'champion',
                     'community', 'comrade', 'contributor', 'crowd', 'defender', 'devotee',
                     'disciple', 'donor', 'enthusiast', 'expert', 'fan', 'fiend', 'follower',
                     'freak', 'groupie', 'guest', 'helper', 'home', 'insider', 'junkie',
                     'local', 'lover', 'maniac', 'member', 'partisan', 'proponent', 'sponsor',
                     'subscriber', 'supporter', 'together', 'us', 'votary', 'voter', 'we']
    
    emotion_list  = ['acclaim', 'accolade', 'admire', 'admire', 'applaud', 'appreciate', 'bravo',
                     'cheer', 'clap', 'comeback', 'congratulate', 'encourage', 'extol', 'fire',
                     'fun', 'glorify', 'hail', 'holler', 'honor', 'hoop', 'huzzah', 'improve',
                     'inspire', 'laud', 'laud', 'laurel', 'motivate', 'ovation', 'plaudit',
                     'praise', 'praise', 'rally', 'react', 'rebound', 'resurge', 'revive', 'roar',
                     'root', 'salute', 'scream', 'screech', 'shout', 'shriek', 'toast', 'welcome',
                     'whistle', 'yell']
    
    activity_list = ['arena', 'assembly', 'audience', 'break', 'camp', 'cavalcade', 'celebration',
                     'ceremony', 'chapter', 'colloquium', 'con', 'concert', 'conclave',
                     'conference', 'convention', 'council', 'demo', 'demonstration', 'diet',
                     'display', 'event', 'fair', 'festival', 'forum', 'gathering', 'holiday',
                     'march', 'march', 'meeting', 'pageant', 'parade', 'park', 'platform',
                     'procession', 'respite', 'sojourn', 'spectacle', 'summit', 'symposium',
                     'synod', 'tour', 'trip', 'vacation']
    
    action_list   = ['acquire', 'appear', 'arrange', 'attend', 'award', 'begin', 'behold', 'buy',
                     'celebrate', 'coordinate', 'create', 'examine', 'explore', 'eye', 'feast',
                     'found', 'frequent', 'gaze', 'glimpse', 'go', 'handle', 'hang', 'haunt',
                     'innovate', 'inspect', 'manage', 'mobilize', 'observe', 'orchestrate',
                     'organize', 'originate', 'patronize', 'plan', 'present', 'purchase', 'ready',
                     'regard', 'reserve', 'run', 'scan', 'schedule', 'scrutinize', 'show', 'study',
                     'survey', 'tackle', 'view', 'visit', 'watch', 'witness']
    
    subcult_list  = ['alienate', 'alternative', 'amateur', 'bizarre', 'counterculture', 'cult',
                     'curate', 'eccentric', 'exotic', 'fresh', 'iconoclast', 'independent',
                     'indie', 'insider', 'irregular', 'mainstream', 'new', 'nonconformist',
                     'novel', 'offbeat', 'open', 'ostentatious', 'outlandish', 'outsider',
                     'provocative', 'radical', 'rebel', 'revolutionary', 'scary', 'scene',
                     'secret', 'social', 'subversive', 'tribe', 'uncommon', 'underground',
                     'unusual', 'urban', 'vibe']
    
    team_count     = 0
    emotion_count  = 0
    activity_count = 0
    action_count   = 0
    subcult_count  = 0
    keywords       = 0
    l              = []

    for word in team_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in emotion_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in activity_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in action_list:
        word = nltk.PorterStemmer().stem(word)    
        
    for word in subcult_list:
        word = nltk.PorterStemmer().stem(word)    
        
    for word in article_text:
        if word in team_list:
            team_count     += 1
        elif word in emotion_list:
            emotion_count  += 1
        elif word in activity_list:
            activity_count += 1
        elif word in action_list:
            action_count   += 1
        elif word in subcult_list:
            subcult_count  += 1
            
    keywords = team_count + emotion_count + activity_count + action_count + subcult_count 
    
    if display == True:            
        if keywords > 0:
            print(team_count/len(article_text), emotion_count/len(article_text),
                  activity_count/len(article_text), action_count/len(article_text),
                  subcult_count/len(article_text), fileid)
        else:
            print('No fan keywords detected.',fileid)     
    
    if len(article_text) > 0:
        l.append(team_count/len(article_text))
        l.append(emotion_count/len(article_text)) 
        l.append(activity_count/len(article_text))
        l.append(action_count/len(article_text))
        l.append(subcult_count/len(article_text))
        return l
    else:
        l = [0,0,0,0,0]
        return l
    
def objsubj(article_text,fileid,display):
    #objective and subjective phrase lists
    obj_list    = ['accept', 'agree', 'allow', 'alter', 'announce', 'approve', 'argue',
                   'assert', 'assign', 'authority', 'begin', 'benefit', 'bill', 'brief',
                   'career', 'charity', 'comment', 'commit', 'conflict', 'consult', 'cost',
                   'court', 'data', 'day', 'deal', 'decision', 'did', 'direct', 'disclose',
                   'donate', 'drop', 'email', 'employ', 'end', 'enforce', 'engage', 'enroll',
                   'evidence', 'express', 'fact', 'fax', 'file', 'focus', 'force', 'group',
                   'halt', 'hour', 'interpret', 'joke', 'law', 'legal', 'legislation', 'limit',
                   'message', 'month', 'not', 'number', 'own', 'pact', 'paper', 'past', 'permit',
                   'platform', 'post', 'predict', 'pressure', 'previous', 'print', 'proof',
                   'publish', 'react', 'receive', 'recent', 'recipient', 'release', 'remind',
                   'report', 'request', 'respond', 'response', 'restrict', 'reveal', 'rise',
                   'said', 'say', 'send', 'sent', 'show', 'solve', 'speech', 'statistic', 'stop',
                   'term', 'time', 'today', 'tomorrow', 'treaty', 'try', 'tweet', 'veto', 'vote',
                   'write', 'year', 'yesterday']
    
    subj_list   = ['actor', 'admonish', 'afraid', 'against', 'alliance', 'ally', 'alternative',
                   'ambitious', 'anger', 'angry', 'assume', 'attempt', 'bad', 'behave', 'belief',
                   'believe', 'best', 'boss', 'bust', 'buttoned', 'care', 'claim', 'class',
                   'complain', 'concern', 'concern', 'condemn', 'conspiracy', 'conspire',
                   'controversy', 'credo', 'creed', 'creep', 'crisis', 'critic', 'dark', 'deep',
                   'denounce', 'disappoint', 'disaster', 'discord', 'dissent', 'distort', 'doom',
                   'eager', 'echo', 'edgy', 'effusive', 'embarrass', 'extreme', 'fake',
                   'fantasy', 'fastidious', 'fear', 'free', 'fundamental', 'gaffe', 'give',
                   'goal', 'good', 'great', 'greed', 'harangue', 'harm', 'harsh', 'horrible',
                   'hurt', 'hypocrisy', 'hypocrite', 'idiot', 'illusion', 'inflate', 'insult',
                   'ire', 'just', 'label', 'madman', 'malice', 'mean', 'messy', 'mislead',
                   'misled', 'moral', 'must', 'need', 'offend', 'offense', 'omen', 'ominous',
                   'ordinary', 'panic', 'power', 'private', 'propaganda', 'prove', 'public',
                   'purpose', 'rage', 'realistic', 'reality', 'reasonable', 'rid', 'right',
                   'rough', 'routine', 'ruling', 'scandal', 'scum', 'secret', 'selfish', 'shady',
                   'shall', 'shock', 'should', 'sin', 'sloppy', 'smear', 'social',
                   'sophisticate', 'spar', 'stacked', 'stage', 'stun', 'subordinate', 'support',
                   'team', 'terrible', 'trash', 'troll', 'true', 'truth', 'unable', 'undermine',
                   'valid', 'value', 'vice', 'victim', 'virtue', 'waddle', 'wealth', 'welfare',
                   'will', 'work', 'worst', 'wrong']
    
    obj_count   = 0
    subj_count  = 0
    l           = []
        
    for word in obj_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in subj_list:
        word = nltk.PorterStemmer().stem(word)    
    
    for word in article_text:
        if word in obj_list:
            obj_count  += 1
        elif word in subj_list:
            subj_count += 1
    
    if display == True:
        if obj_count > 1 and subj_count > 1:
            print(obj_count/len(article_text), subj_count/len(article_text), fileid)
        else:
            print('Not enough objective and subjective keywords to analyze',fileid)
    
    if len(article_text) > 0:
        l.append(obj_count/len(article_text))
        l.append(subj_count/len(article_text))
        return l
    else:
        l = [0,0]       
        return l

def psychological_analyzer(article_text,fileid,display):
    #Big 5 traits are openness to experience, conscientiousness, extraversion, agreeableness, neuroticism
    o_high       = ['act', 'adventure', 'anticipation', 'arcade', 'bewitch', 'bivouac', 'bliss',
                    'bliss', 'camp', 'cantonment', 'captivate', 'charm', 'concert', 'current',
                    'delight', 'display', 'dream', 'ecstasy', 'elation', 'enchant', 'entertain',
                    'entrance', 'euphoria', 'event', 'excite', 'exhibit', 'exhiliration',
                    'experience', 'feat', 'festival', 'fresh', 'gallery', 'gig', 'gladden',
                    'gratify', 'happy', 'hike', 'imaginative', 'invigor', 'journey', 'joy',
                    'latest', 'latest', 'lively', 'marvel', 'modern', 'museum', 'new', 'novel',
                    'original', 'outing', 'performance', 'piece', 'play', 'pleasure',
                    'presentation', 'production', 'rapture', 'recent', 'recital', 'risk', 'rove',
                    'rush', 'scene', 'sensation', 'show', 'sprightly', 'spry', 'staging', 'tent',
                    'thrill', 'tingle', 'traipse', 'trek', 'vibrant', 'vital', 'zing']
    
    o_low        = ['abnormal', 'alien', 'anomalous', 'atrocious', 'atypical', 'bizarre',
                    'coarse', 'controversial', 'crude', 'danger', 'deviant', 'different',
                    'disgusting', 'evil', 'explicit', 'extraordinary', 'extreme', 'fool', 'freak',
                    'heinous', 'hideous', 'immoral', 'improper', 'indecent', 'infrequent',
                    'irregular', 'isolated', 'lewd', 'objectionable', 'obscene', 'occasional',
                    'odd', 'odious', 'offbeat', 'offensive', 'outlandish', 'outrageous',
                    'peculiar', 'quirk', 'rare', 'repugnant', 'rude', 'salacious', 'shocking',
                    'singular', 'sporadic', 'strange', 'surprising', 'unacceptable',
                    'unconventional', 'uncustomary', 'unexpected', 'unfamiliar', 'unorthodox',
                    'unusual', 'unwonted', 'vulgar', 'wacky', 'weird', 'wicked']
    
    c_high       = ['achieve', 'application', 'assign', 'authority', 'blueprint', 'calling',
                    'career', 'chore', 'commission', 'continue', 'control', 'course', 'creation',
                    'curriculum', 'design', 'diagram', 'direction', 'discipline', 'drill',
                    'drudge', 'duty', 'effort', 'employ', 'exertion', 'flow', 'grind', 'industry',
                    'instruct', 'job', 'labor', 'lecture', 'map', 'meet', 'moil', 'occupation',
                    'order', 'persevere', 'persist', 'plan', 'position', 'practice', 'praxis',
                    'prepare', 'profession', 'project', 'regimen', 'regulate', 'regulation',
                    'research', 'routine', 'schedule', 'scheme', 'service', 'situation', 'skill',
                    'slog', 'strategy', 'study', 'subject', 'sweat', 'system', 'tactic', 'task',
                    'teach', 'timetable', 'toil', 'trade', 'train', 'travail', 'vocation', 'work']
    
    c_low        = ['abandon', 'abjure', 'adjourn', 'boring', 'break', 'breather', 'brunch',
                    'cancel', 'cease', 'cede', 'chaos', 'clutter', 'conclude', 'confusion',
                    'debris', 'defer', 'delay', 'desert', 'detritus', 'dinner', 'disarray',
                    'disheveled', 'disorganize', 'dissolve', 'ditch', 'doze', 'drop', 'drudge',
                    'dull', 'dump', 'finish', 'flee', 'fragments', 'furlough', 'halt', 'hold',
                    'holiday', 'jettison', 'jilt', 'jumble', 'junk', 'lazy', 'leave', 'litter',
                    'lost', 'lunch', 'mess', 'miscellany', 'mishmash', 'missing', 'muddle', 'nap',
                    'pause', 'postpone', 'prorogue', 'recess', 'relinquish', 'rest', 'retreat',
                    'rubbish', 'scatter', 'scrap', 'scrub', 'search', 'shambles', 'shelve',
                    'slack', 'slumber', 'snack', 'snooze', 'sojourn', 'staycation', 'strand',
                    'surrender', 'tangle', 'tour', 'trash', 'trip', 'untidy', 'vacation', 'vacay',
                    'withdraw', 'yield']
    
    e_high       = ['affiliate', 'affinity', 'alliance', 'ally', 'assemblage', 'associate',
                    'band', 'bevy', 'bloc', 'brotherhood', 'bunch', 'chorus', 'circle', 'class',
                    'clique', 'club', 'coalition', 'collective', 'combine', 'common', 'company',
                    'concord', 'concur', 'confidant', 'consensus', 'consent', 'consortium',
                    'couple', 'crew', 'crowd', 'ensemble', 'fraternity', 'friends', 'gaggle',
                    'gang', 'gather', 'group', 'guild', 'harmony', 'horde', 'huddle', 'intimate',
                    'jointly', 'league', 'majority', 'mob', 'multitude', 'network',
                    'organization', 'our', 'pack', 'pact', 'partner', 'party', 'posse', 'ring',
                    'sisterhood', 'society', 'solidarity', 'sorority', 'soulmate', 'squad',
                    'swarm', 'syndicate', 'tandem', 'team', 'throng', 'together', 'treaty',
                    'troupe', 'unanimity', 'union', 'unison', 'unity', 'us', 'we', 'you']
    
    e_low        = ['I', 'abandoned', 'alone', 'anchoress', 'anchorite', 'antisocial', 'ascetic',
                    'avoidant', 'cloistered', 'companionless', 'cutoff', 'deserted', 'desolate',
                    'distant', 'eremite', 'forgotten', 'forlorn', 'forsaken', 'hermit',
                    'hikikomori', 'incommunicado', 'independent', 'individual', 'introvert',
                    'isolated', 'lone', 'me', 'mine', 'misanthrope', 'myself', 'neglected',
                    'outlying', 'partnerless', 'pillarist', 'private', 'recluse', 'remote',
                    'reserved', 'retiring', 'secluded', 'seclusion', 'single', 'sole', 'solitary',
                    'solo', 'solus', 'stranded', 'stylite', 'unaccompanied', 'unaided',
                    'unassisted', 'unattended', 'unescorted', 'unforthcoming', 'unsociable',
                    'vacant', 'withdrawn']
    
    a_high       = ['acclaim', 'acclamation', 'accolade', 'acknowledge', 'address', 'admire',
                    'adore', 'adulation', 'affirm', 'agree', 'allow', 'applaud', 'applause',
                    'appreciate', 'approbation', 'approve', 'authorize', 'award', 'ballyhoo',
                    'bless', 'celebrate', 'cheer', 'clap', 'commend', 'compliment',
                    'congratulate', 'contribute', 'encomium', 'encourage', 'endorse', 'enliven',
                    'eulogy', 'exalt', 'exhilarate', 'flatter', 'gladden', 'glorify', 'greet',
                    'hail', 'hallow', 'hearten', 'hello', 'honor', 'inspire', 'joy', 'kudos',
                    'laud', 'laurel', 'like', 'lionize', 'love', 'motivate', 'ovation',
                    'panegyrize', 'praise', 'proclaim', 'puffery', 'ratify', 'recommend',
                    'rejoice', 'revere', 'reward', 'right', 'salute', 'share', 'support',
                    'treasure', 'tribute', 'uphold', 'uplift', 'validate', 'venerate', 'vote',
                    'welcome']
    
    a_low        = ['afflict', 'afters', 'altercation', 'antagonism', 'argue', 'ban', 'battle',
                    'brawl', 'challenge', 'chastise', 'clash', 'concoct', 'condone', 'conflict',
                    'contention', 'contest', 'controversy', 'cynic', 'deceit', 'disagree',
                    'disallow', 'discord', 'disloyal', 'dispute', 'dissension', 'dissent',
                    'distort', 'donnybrook', 'doubt', 'duplicity', 'expose', 'fabricate', 'fake',
                    'fallacy', 'false', 'feud', 'fickle', 'fight', 'flaw', 'fracas', 'friction',
                    'halt', 'hinder', 'hit', 'imprecise', 'inaccurate', 'incite', 'inconstant',
                    'incorrect', 'miff', 'opposition', 'prevent', 'problem', 'prohibit', 'punish',
                    'quarrel', 'row', 'scrap', 'shady', 'shallow', 'sketchy', 'spat', 'squabble',
                    'stop', 'strife', 'tiff', 'traitor', 'unreliable', 'untrue', 'variance',
                    'wicked', 'wily', 'wrangle', 'wrong']
    
    n_high       = ['agitate', 'alarm', 'antsy', 'anxiety', 'anxious', 'apprehensive', 'bearish',
                    'bother', 'brittle', 'challenge', 'complex', 'concern', 'consequences',
                    'craze', 'cry', 'desperate', 'difficult', 'discombombulate', 'disconcert',
                    'disquiet', 'distraught', 'distress', 'disturb', 'dither', 'edgy', 'escape',
                    'faze', 'fear', 'fever', 'flight', 'fluster', 'frantic', 'fraught', 'frenzy',
                    'fret', 'fright', 'hysteric', 'inflame', 'issue', 'jumpy', 'mad', 'misery',
                    'mousy', 'nervous', 'overwrought', 'panic', 'paranoid', 'perturb', 'question',
                    'rattle', 'restless', 'stress', 'struggle', 'tense', 'threat', 'timid',
                    'tizzy', 'trouble', 'uncontrolled', 'uneasy', 'unhinged', 'unsettle',
                    'unsure', 'victim', 'worry']
    
    n_low        = ['affable', 'aplomb', 'assertive', 'assure', 'backbone', 'bold', 'brave',
                    'bullish', 'buoyant', 'calm', 'casual', 'clear', 'comfort', 'composed',
                    'conclusive', 'confident', 'conviction', 'courage', 'credence', 'cushy',
                    'determined', 'easy', 'easygoing', 'effortless', 'elementary', 'endure',
                    'facile', 'fact', 'faith', 'familiar', 'fearless', 'feisty', 'fortitude',
                    'grit', 'hope', 'informal', 'insouciant', 'mettle', 'natural', 'nonchalant',
                    'optimistic', 'painless', 'peace', 'pluck', 'poise', 'positive', 'relax',
                    'reliance', 'relief', 'rely', 'resilient', 'resolution', 'resolve', 'safe',
                    'sanguine', 'secure', 'serene', 'simple', 'spirit', 'steadfast',
                    'straightforward', 'suave', 'tranquil', 'truth', 'unafraid', 'unceremonious',
                    'unchallenging', 'unconcerned', 'undemanding', 'understand', 'undisturbed',
                    'unflappable', 'unreserved', 'untroubled', 'upbeat', 'urbane', 'valid',
                    'will']
    
    o_high_count = 0
    o_low_count  = 0
    c_high_count = 0
    c_low_count  = 0
    e_high_count = 0
    e_low_count  = 0
    a_high_count = 0
    a_low_count  = 0
    n_high_count = 0
    n_low_count  = 0
    trait_total  = 0
    l            = []
    
    #stemming
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
    
    for word in o_high:
        word = nltk.PorterStemmer().stem(word)
    for word in o_low:
        word = nltk.PorterStemmer().stem(word)
    for word in c_high:
        word = nltk.PorterStemmer().stem(word)
    for word in c_low:
        word = nltk.PorterStemmer().stem(word)
    for word in e_high:
        word = nltk.PorterStemmer().stem(word)
    for word in e_low:
        word = nltk.PorterStemmer().stem(word)
    for word in a_high:
        word = nltk.PorterStemmer().stem(word)
    for word in a_low:
        word = nltk.PorterStemmer().stem(word)
    for word in n_high:
        word = nltk.PorterStemmer().stem(word)
    for word in n_low:
        word = nltk.PorterStemmer().stem(word)
    
    #analyze each trait
    for word in article_text:
        if word in o_high:
            o_high_count += 1
        elif word in o_low:
            o_low_count  += 1
        elif word in c_high:
            c_high_count += 1
        elif word in c_low:
            c_low_count  += 1
        elif word in e_high:
            e_high_count += 1
        elif word in e_low:
            e_low_count  += 1
        elif word in a_high:
            a_high_count += 1
        elif word in a_low:
            a_low_count  += 1
        elif word in n_high:
            n_high_count += 1
        elif word in n_low:
            n_low_count  += 1
            
    #add up total scores for each trait    
    trait_total = (o_high_count + o_low_count + c_high_count + c_low_count + e_high_count +
    e_low_count + a_high_count + a_low_count + n_high_count + n_low_count)
    
    if display == True:
        if trait_total > 0:
            k = 100 / trait_total
            print((o_high_count - o_low_count) * k, (c_high_count - c_low_count) * k,
                  (e_high_count - e_low_count) * k, (a_high_count - a_low_count) * k,
                  (n_high_count - n_low_count) * k, fileid)
        else:
            print('No psychological keywords to analyze',fileid)
    
    if trait_total > 0:
        k = 100 / trait_total
        l.append((o_high_count - o_low_count) * k)
        l.append((c_high_count - c_low_count) * k)
        l.append((e_high_count - e_low_count) * k)
        l.append((a_high_count - a_low_count) * k)
        l.append((n_high_count - n_low_count) * k)
        return l
    else:
        l = [0,0,0,0,0]
        return l
    
def urgency_analyzer(article_text,fileid,display):
    
    urg_list = ['abrupt', 'acute', 'alacrity', 'alert', 'anticipated', 'apprise', 'approach',
                'asap', 'barrel', 'bolt', 'boogie', 'bound', 'brewing', 'clip', 'close', 'compel',
                'contemporary', 'crucial', 'current', 'dash', 'desperate', 'dire', 'drastic',
                'essential', 'exhort', 'exigent', 'expected', 'expedite', 'extreme', 'fickle',
                'flash', 'fleet', 'float', 'fluctuate', 'forthcoming', 'fresh', 'gallop', 'grave',
                'haste', 'hurry', 'hurtle', 'immediate', 'imminent', 'impacting', 'impend',
                'imperative', 'important', 'inform', 'instant', 'intense', 'late', 'latter',
                'lightning', 'looming', 'mandatory', 'menacing', 'modern', 'move', 'mutable',
                'near', 'necessary', 'neoteric', 'new', 'nigh', 'notify', 'now', 'overnight',
                'parlous', 'pressing', 'priority', 'prompt', 'quick', 'rapid', 'rathe', 'remind',
                'run', 'rush', 'scamper', 'scoot', 'scramble', 'scurry', 'scuttle', 'serious',
                'shake', 'shift', 'snap', 'soon', 'speed', 'sudden', 'tell', 'test', 'threatening',
                'tip', 'uncertain', 'upcoming', 'urge', 'vacillate', 'vary', 'vital', 'volatile',
                'warn', 'whip', 'whirlwind', 'zip', 'zoom']
    
    urg_score = 0
    urg_count = 0
    l         = []
        
    for word in urg_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in article_text:
        if word in urg_list:
            urg_count += 1
    
    if display == True:    
        if len(article_text) > 0 and urg_count > 0:
            urg_score = urg_count/len(article_text)
            print(urg_score,fileid)
        else:
            print('No urgency keywords were detected.')

    if len(article_text) > 0:
        urg_score = urg_count/len(article_text)
        l.append(urg_score)
        return l
    else:
        l = [0]
        return l

def status_analyzer(article_text,fileid,display):
    status_words = 0
    status_list  = ['acclaim', 'aesthetic', 'allude', 'appall', 'arguably', 'beautiful', 'bridging',
                   'calculation', 'canvas', 'capstone', 'chaperone', 'commemorate', 'commentary',
                   'commentator', 'complement', 'complement', 'complicate', 'consistency',
                   'continuation', 'convey', 'culinary', 'curator', 'delve', 'delving',
                   'devastating', 'divulge', 'divulging', 'duration', 'dynamic', 'emerge',
                   'emphasize', 'empower', 'enormous', 'essential', 'exhibit', 'exhilarating',
                   'expansive', 'fascinating', 'ferment', 'finery', 'flexibility', 'former',
                   'fortunate', 'frivolous', 'fulfill', 'fuzzy', 'genuine', 'gratify', 'hesitancy',
                   'imbalance', 'influx', 'infuse', 'innate', 'innumerable', 'inquiries',
                   'inquisitive', 'inspire', 'integral', 'intend', 'intense', 'interpersonal',
                   'interpret', 'introspection', 'invaluable', 'limitless', 'local', 'measly',
                   'motivating', 'necessarily', 'necessitate', 'palette', 'passionate', 'pastime',
                   'perspective', 'precise', 'primarily', 'profound', 'prohibitive', 'proliferate',
                   'reaffirm', 'recreation', 'recyclable', 'redefining', 'reflection', 'resolve',
                   'resolving', 'revere', 'revering', 'rigorous', 'scholastic', 'sculpt',
                   'shockingly', 'sopping', 'splashy', 'substantive', 'substantive', 'succinct',
                   'supercilious', 'sustainable', 'tertiary', 'thoughtful', 'trajectory',
                   'untapped', 'venture', 'vitality', 'vocalize', 'wither']
    l            = []
    
    for word in status_list:
        word = nltk.PorterStemmer().stem(word)
    
    for word in article_text:
        if word in status_list:
            status_words += 1
    
    if display == True:
        if status_words > 0 and len(article_text) > 0:
            print(status_words/len(article_text),fileid)
        else:
            print('No status keywords to analyze.')
    
    if len(article_text) > 0:        
        l.append(status_words/len(article_text))
        return l
    else:
        l = [0]
        return l
 
def narrative_analyzer(article_text,fileid,display):
    
    narrative = [('a', 'story'), ('an', 'affiliation'), ('and', 'until'), ('are', 'introducing'),
                 ('are', 'often'), ('are', 'seeing'), ('at', 'least'), ('be', 'seen'),
                 ('been', 'around'), ('been', 'related'), ('common', 'buzzwords'),
                 ('common', 'terms'), ('conversations', 'about'), ('countless', 'number'),
                 ('currently', 'happening'), ('deeply', 'felt'), ('double', 'down'),
                 ('existed', 'for'), ('familiarity', 'with'), ('frequently', 'used'),
                 ('from', 'the'), ('going', 'to'), ('greatest', 'hits'), ('has', 'happened'),
                 ('has', 'repeatedly'), ('has', 'seen'), ('has', 'stories'), ('have', 'been'),
                 ('have', 'spawned'), ('heard', 'examples'), ('important', 'issue'),
                 ('is', 'growing'), ('isolated', 'incident'), ('last', 'few'), ('last', 'round'),
                 ('like', 'this'), ('most', 'of'), ('most', 'often'), ('most', 'recent'),
                 ('multiple', 'times'), ('next', 'time'), ('number', 'of'), ('of', 'approach'),
                 ('of', 'this'), ('often', 'used'), ('originally', 'used'), ('other', 'trend'),
                 ('over', 'the'), ('previously', 'indicated'), ('primarily', 'used'),
                 ('proclivity', 'to'), ('recent', 'months'), ('remember', 'all'), 
                 ('serious', 'issue'), ('several', 'times'), ('similarly', 'intense'),
                 ('some', 'aspects'), ('some', 'stories'), ('started', 'being'),
                 ('starting', 'to'), ('suggests', 'that'), ('that', 'approach'), ('that', 'trend'),
                 ('the', 'bulk'), ('the', 'extent'), ('these', 'issues'), ('these', 'situations'),
                 ('this', 'focus'), ('this', 'issue'), ('this', 'subject'), ('this', 'trend'),
                 ('we', 'see'), ('when', 'situations'), ('whenever', 'someone'), ('widely', 'read'),
                 ('will', 'continue')]
    
    n_score      = 0
    l            = []
    
    article_text = list(nltk.bigrams(article_text))
    
    for pair in narrative:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
            
    for pair in article_text:
        if pair in narrative:
            n_score += 1
    
    if display == True:
        if n_score > 0:
            print(n_score/len(article_text),fileid)
        else:
            print('Not enough narrative keywords to analyze.')
    
    if n_score > 0:        
        l.append(n_score/len(article_text))
        return l
    else:
        l = [0]
        return l        
    
def addition_analyzer(article_text,fileid,display):
    addition_list    = [('accessory', 'to'), ('added', 'to'), ('added', 'to'), ('along', 'with'),
                        ('ancillary', 'to'), ('and', 'all'), ('and', 'also'), ('annexed', 'to'),
                        ('apart', 'from'), ('appended', 'to'), ('as', 'companion'), ('as', 'well'),
                        ('attached', 'to'), ('auxiliary', 'to'), ('but', 'still'),
                        ('conjunction', 'with'), ('coupled', 'with'), ('even', 'more'),
                        ('greater', 'extent'), ('in', 'addition'), ('in', 'like'),
                        ('lesser', 'extent'), ('more', 'about'), ('more', 'crucial'),
                        ('more', 'germane'), ('more', 'important'), ('more', 'indispensable'),
                        ('more', 'key'), ('more', 'notable'), ('more', 'pertinent'),
                        ('more', 'relevant'), ('more', 'salient'), ('more', 'significant'),
                        ('more', 'useful'), ('more', 'valuable'), ('more', 'vital'),
                        ('not', 'only'), ('on', 'top'), ('once', 'again'), ('same', 'argument'),
                        ('same', 'concept'), ('same', 'idea'), ('same', 'time'), ('same', 'token'),
                        ('same', 'topic'), ('same', 'way'), ('supplementary', 'to'),
                        ('supportive', 'of'), ('to', 'boot'), ('together', 'with'), ('with', 'that')]
    
    addition_count = 0
    l              = []
    
    article_text = list(nltk.bigrams(article_text))
    
    for pair in article_text:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
    
    for pair in addition_list:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
    
    for pair in article_text:
        if pair in addition_list:
            addition_count += 1
    
    if display == True:        
        if addition_count > 0:
            print(addition_count/len(article_text),fileid)
        else:
            print('No supporting keywords detected.')
            
    if addition_count > 0:
        l.append(addition_count/len(article_text))
        return l
    else:
        l = [0]
        return l        
    
def automatedreadability(chars,article_text,sents,fileid,display):
    #If this section doesn't work remove the BOM from the UTF-8 text files
    #Bomremover is a good program for doing this
    index = 0
    l     = []
    
    if display == True:
        if len(sents) > 0:
            index = (4.71 * len(chars)/len(article_text)) + 0.5 * (len(article_text)/len(sents) - 21.43)
            print(index,fileid)
        else:
            print('Not enough text to analyze.')
    
    if len(sents) > 0: 
        index = (4.71 * len(chars)/len(article_text)) + 0.5 * (len(article_text)/len(sents) - 21.43)
        l.append(index)
        return l
    else:
        l = [0]
        return l        
    
def news_analyzer(article_text,fileid,display):
        
    #sourcing, quantitativeness, certainty
    
    s_tuples = [('a','letter'), ('an','article'), ('accepted','responsibility'),
                ('according', 'to'), ('admitted','to'), ('after','the'), ('appears','in'),
                ('article', 'in'), ('appendices', 'contain'), ('asserted','by'), ('before','the'),
                ('characterized','by'), ('cited','by'), ('corpus','of'), ('described', 'in'),
                ('describing','the'), ('denied','the'), ('developed', 'in'), ('disclosed','by'),
                ('documents','are'), ('et','al'), ('given','by'), ('he','predicted'), ('he','said'),
                ('in','literature'), ('journal','article'), ('look','at'), ('main','contribution'),
                ('many','studies'), ('look','up'), ('models','study'), ('motivated','by'),
                ('numerous','models'), ('observations','are'), ('obtained', 'by'), ('posted','by'),
                ('produced','by'), ('provided','by'), ('reference', 'to'), ('referring','to'),
                ('reported','by'), ('report', 'in'), ('supported', 'by'), ('terms','of'),
                ('the','report'),  ('the','source'), ('these','strategies'), ('seminal','work'), 
                ('she','said'), ('she','predicted'), ('supposed','to'), ('recent','work'),
                ('reference', 'article'), ('relevant','information'), ('report','included'),
                ('research','paper'), ('the','conclusion'), ('the','relevant'), ('the','papers'),
                ('this','citation'), ('this','paper'), ('this','review'), ('this','study'),
                ('white','paper')]
    
    q_tuples = [('a','limit'), ('a','model'), ('above','example'), ('analyze','the'),
                ('apply','the'), ('average','of'), ('basic','familiarity'), ('basic','knowledge'),
                ('basic','understanding'), ('be','solved'), ('chart','the'), ('calculate','the'),
                ('compute','the'), ('define','the'), ('determine','the'), ('elementary','concepts'),
                ('elementary','knowledge'), ('empirically','study'), ('establishes','that'),
                ('estimate','the'), ('example','arises'), ('expect','that'), ('expected','value'),
                ('experimental','results'), ('experiments','show'), ('find','out'), ('has','to'),
                ('have','to'), ('is','called'), ('in','comparison'), ('is','described'),
                ('is','related'), ('is','satisfied'), ('is','sufficient'), 
                ('mathematical','definition'), ('mathematical','terms'), ('not','necessarily'),
                ('often','formulated'), ('only','if'), ('popular','approach'),
                ('preceding', 'examples'), ('precise','definition'), ('plot','the'),
                ('proceeds','by'), ('prove','that'), ('proposed','algorithm'), ('proposed','model'),
                ('shown','that'), ('study','conditions'), ('solve','problems'), 
                ('systematic', 'review'), ('that','correlated'), ('the','concept'),
                ('the','construction'), ('the','data'), ('the','expected'), ('the','performance'),
                ('the','quantities'), ('theory','of'), ('these','models'), ('thus','obtain'),
                ('to','consider'), ('to','derive'), ('to','determine'), ('to','examine'),
                ('to','replicate'), ('to','solve'), ('to','solving'), ('useful','to'),
                ('verifies','that')]
    
    c_tuples = [('a','conclusion'), ('a','solution'), ('acknowledged','that'), ('agree','that'),
                ('analyzing','that'), ('are','computed'), ('are','repeated'), ('assume','that'),
                ('be','combined'), ('be','extended'), ('becomes','clear'), ('believe','that'),
                ('clear','that'), ('do','exist'), ('either','observe'), ('empirical','studies'),
                ('establish','that'), ('estimate','the'), ('essentially','leads'), 
                ('evidence','that'), ('existence','of'),  ('facilitate','the'),
                ('feedback','effects'), ('finally','state'), ('first','explanations'),
                ('forecast','that'), ('forecast','the'), ('fully','describe'), ('fully','explain'),
                ('if','they'), ('if','we'), ('implies','that'), ('in','context'), ('in','contrast'),
                ('in','particular'), ('initial','choice'), ('into','account'), ('is','necessary'),
                ('is','statistically'), ('it','describes'), ('make','decisions'),
                ('method','approximates'), ('new','insights'), ('no','assumption'), ('note','that'),
                ('observe','that'), ('original','model'), ('our','experiments'),
                ('outperforms','the'), ('restricting','attention'), ('results','obtained'),
                ('see','that'), ('seem','to'), ('specific','assumption'),
                ('sufficient','information'), ('suggesting','that'), ('the','context'), 
                ('the','importance'), ('the','outcomes'), ('the','results'), 
                ('theoretical','models'), ('these','conditions'), ('they','imply'), ('this','case'),
                ('this','implies'), ('this','situation'), ('underlying','uncertainty'),
                ('wider','consequences'), ('wider','effects')]
    
    s_score  = 0
    q_score  = 0
    c_score  = 0
    l        = []

    article_text = list(nltk.bigrams(article_text))
    
    for pair in s_tuples:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
            
    for pair in q_tuples:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
            
    for pair in c_tuples:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
    
    for pair in article_text:
        if pair in s_tuples:
            s_score += 1
        elif pair in q_tuples:
            q_score += 1
        elif pair in c_tuples:
            c_score += 1
    
    if display == True:        
        if (s_score + q_score + c_score) != 0:
            print(s_score/len(article_text),q_score/len(article_text),c_score/len(article_text),fileid)    
        else:
            print('No keywords detected.')
    
    if (s_score + q_score + c_score) != 0:
        l.append(s_score/len(article_text))
        l.append(q_score/len(article_text))
        l.append(c_score/len(article_text))
        return l
    else:
        l = [0,0,0]
        return l

def stops(article_text,fileid,display):
    #Calculate ratio of stopwords to other text
    stoplist  = set(nltk.corpus.stopwords.words('english'))
    stopcount = 0
    stopscore = 0
    l         = []
    
    for word in article_text:
        if word in stoplist:
            stopcount += 1   
    
    if display == True:
        if len(article_text) > 0:
            stopscore = stopcount / len(article_text)
            print(stopscore,fileid)
        else:
            print('Not enough text to analyze.')
    
    if len(article_text) > 0: 
        stopscore = stopcount / len(article_text)
        l.append(stopscore)
        return l
    else:
        l = [0]
        return l

def storytelling(article_text,fileid,display):
    #Calculate ratio between statements that change the story, or result from prior events.
    cause_list  = ['although', 'apart', 'bar', 'because', 'but', 'despite', 'disregard',
                   'except', 'exclude', 'exempt', 'however', 'leave', 'nevertheless',
                   'nonetheless', 'not', 'omit', 'other', 'outside', 'save', 'still',
                   'therefore', 'though', 'yet']                 
    
    follow_list = ['above', 'addition', 'along', 'also', 'and', 'any', 'besides', 'combine',
                   'equal', 'expand', 'extra', 'further', 'include', 'increase', 'more',
                   'more', 'multiply', 'over', 'plus', 'supplement', 'then', 'too', 'with']
    
    cause_count   = 0
    follow_count  = 0
    l             = []
        
    for word in cause_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in follow_list:
        word = nltk.PorterStemmer().stem(word)    
    
    for word in article_text:
        if word in cause_list:
            cause_count  += 1
        elif word in follow_list:
            follow_count += 1
    
    if display == True:
        if cause_count > 1 and follow_count > 1:
            print(cause_count/len(article_text), follow_count/len(article_text), fileid)
        else:
            print('Not enough change and result keywords to analyze',fileid)
    
    if len(article_text) > 0:
        l.append(cause_count/len(article_text))
        l.append(follow_count/len(article_text))
        return l
    else:
        l = [0,0]       
        return l
    
def cooperation(article_text,fileid,display):
    
    family_list = ['blood', 'brood', 'child', 'clan', 'descendant', 'father', 'folk', 'heir',
                   'issue', 'kin', 'mother', 'parent', 'progeny', 'relative', 'scion', 'pack',
                   'tribe']
    
    group_list  = ['adhere', 'believe', 'bond', 'commit', 'constant', 'dedicate', 'depend',
                   'devote', 'duty', 'faith', 'loyal', 'patriot', 'rely', 'stable', 'true',
                   'trust']
    
    recip_list  = ['amend', 'barter', 'forgive', 'reciprocity', 'remunerate', 'reparation',
                   'repay', 'requit', 'satisfy', 'substitute', 'trade']
    
    hero_list   = ['audacious', 'bold', 'courage', 'daring', 'gallant', 'grit', 'hardiness',
                   'hero', 'intrepid', 'mettle', 'moxie', 'nerve', 'spine', 'spirit', 'valor']
    
    defer_list  = ['comply','tract','defer','duty','respect','conform','obey','submit',
                   'passive','meek','subservient','tame','pliable','docile']
    
    fair_list   = ['comply', 'conform', 'defer', 'docile', 'duty', 'meek', 'obey', 'passive',
                   'pliable', 'respect', 'submit', 'subservient', 'tame', 'tract']
    
    prop_list   = ['asset', 'capital', 'claim', 'estate', 'fortune', 'hold', 'land', 'own',
                   'portfolio', 'possess', 'property', 'stake', 'title', 'wealth']
    
    fam_score   = 0
    gro_score   = 0
    rec_score   = 0
    her_score   = 0
    def_score   = 0
    fai_score   = 0
    pro_score   = 0
    total       = 0
    l           = []

    for word in family_list:
        word = nltk.PorterStemmer().stem(word)
    for word in group_list:
        word = nltk.PorterStemmer().stem(word)    
    for word in recip_list:
        word = nltk.PorterStemmer().stem(word)    
    for word in hero_list:
        word = nltk.PorterStemmer().stem(word)    
    for word in defer_list:
        word = nltk.PorterStemmer().stem(word)
    for word in fair_list:
        word = nltk.PorterStemmer().stem(word)
    for word in prop_list:
        word = nltk.PorterStemmer().stem(word)    
        
    for word in article_text:
        if word in family_list:
            fam_score += 1
        if word in group_list:
            gro_score += 1
        if word in recip_list:    
            rec_score += 1
        if word in hero_list:    
            her_score += 1
        if word in defer_list:    
            def_score += 1
        if word in fair_list:
            fai_score += 1
        if word in prop_list:
            pro_score += 1

    total = fam_score + gro_score + rec_score + her_score + def_score + fai_score + pro_score
    
    if display == True:            
        if total > 0:
            print(fam_score/len(article_text), gro_score/len(article_text),
                  rec_score/len(article_text), her_score/len(article_text),
                  def_score/len(article_text), fai_score/len(article_text),
                  pro_score/len(article_text), fileid)
        else:
            print('No cooperation keywords detected.',fileid)     
    
    if len(article_text) > 0:
        l.append(fam_score/len(article_text))
        l.append(gro_score/len(article_text)) 
        l.append(rec_score/len(article_text))
        l.append(her_score/len(article_text))
        l.append(def_score/len(article_text))
        l.append(fai_score/len(article_text))
        l.append(pro_score/len(article_text))
        return l
    else:
        l = [0,0,0,0,0,0,0]
        return l
    
main()
    